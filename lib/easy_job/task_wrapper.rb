module EasyJob
  class TaskWrapper
    include Logging

    STATE_PENDING = 0
    STATE_RUNNING = 1
    STATE_FINISHED = 2

    MAX_DB_CONNECTION_ATTEMPTS = 100

    def initialize(task_class, args)
      @task_class = task_class
      @args = args
      @state = STATE_PENDING

      @connection_attempt = 0
      @current_user = User.current
      @current_locale = I18n.locale
    end

    def job_id
      @job && @job.job_id
    end

    def self.job_name
      name
    end

    def job_name
      @task_class.job_name
    end

    def pending?
      @state == STATE_PENDING
    end

    def running?
      @state == STATE_RUNNING
    end

    def finished?
      @state == STATE_FINISHED
    end

    def perform(**options)
      @job = @task_class.new
      @job.job_options = options
      @job.job_id = SecureRandom.uuid

      Thread.current[:easy_job] = { id: @job.job_id }

      ensure_connection {
        ensure_redmine_env {
          begin
            @state = STATE_RUNNING
            @job.perform(*@args)
          rescue => ex
            @job.handle_error(ex)
          ensure
            @state = STATE_FINISHED
            log_info 'Job ended'
          end
        }
      }
    rescue => e
      # Perform method must end successfully.
      # Otherwise `all_done?` end on deadlock.
    ensure
      Thread.current[:easy_job] = nil
    end

    def ensure_connection
      ActiveRecord::Base.connection_pool.with_connection { yield }
    rescue ActiveRecord::ConnectionTimeoutError
      @connection_attempt += 1
      if @connection_attempt > MAX_DB_CONNECTION_ATTEMPTS
        log_error 'Max ConnectionTimeoutError'
        return
      else
        log_warn "ConnectionTimeoutError attempt=#{@connection_attempt}"
        retry
      end
    end

    def ensure_redmine_env
      orig_user = User.current
      orig_locale = I18n.locale
      User.current = @current_user
      I18n.locale = @current_locale
      yield
    ensure
      User.current = orig_user
      I18n.locale = orig_locale
    end

    def inspect
      id = job_id.presence || 'not yet'
      %{#<EasyJob::TaskWrapper(#{@task_class}) id="#{id}">}
    end

  end
end
