module EasyJob
  module ExceptionsNotifier

    def handle_error(ex)
      super
      EasyExtensions::ExceptionsNotifier.notify(ex)
    end

  end
end

EasyJob::Base.prepend(EasyJob::ExceptionsNotifier)
